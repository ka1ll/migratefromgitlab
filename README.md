﻿# Migration script for moving from Gitlab to Gogs/Gitea

[Based on a script by MarcelSimon](https://github.com/MarcelSimon/MigrateGitlabToGogs)


For migration uses Migrate API from Gogs/ Gitea:
------------

* [Gogs-Doc](https://github.com/gogs/docs-api/tree/master/Repositories) 

Requirements
------------

* [Python3](https://www.python.org/downloads/)
* [requests](https://pypi.org/project/requests/)

Usage
------------

  The script accepts two command-line parameters:

#### [wizard] - Run the step-by-step wizard

> `> migrate_from_gitlab_to_gogs.py wizard`

Sorry, not available now... Coming soon

#### [config] - Run the script with the parameters from the file "config.json"

> `> migrate_from_gitlab_to_gogs.py config`

By default, the script will search for the "config.json" in the same folder.
You can specify a different path to the file, please use the --path or -p key

> `> migrate_from_gitlab_to_gogs.py config -p my_specify_folder/config.json`

# So, first...
  Open config.json in a text editor and fill in all fields:

|Name|Example|Description|
|----|----|-----------|
|`source_type`|user|The type can be `user` or `group`. Select a `user` if you want to copy all the projects of your user or select `group` if you want to copy all the projects of your group.|
|`source_type_name`|user_or_group_name_on_gitlab|Enter the name of the user or group on GitLab whose projects you want to migrate. By default, the script will try to save the path. Your must take care of the presence of a similar user (group) on Gogs/Gitea in advance.|
|`source_repo`|http://gitlab.example.com|GitLab URL|
|`gitlab_token`|9koXpg98eAheJpvBs5tK|The AccessToken is used to access to source_repo requests, such as the list of projects of a user or group. Note that the token must have the appropriate rights on GitLab.|
|`auth_username`|user_with_access_rights_to_repo|Authorization username on GitLab with rights to repo|
|`auth_password`|password|Authorization password|
|`git_lab_api_version`|/api/v4|API version on GitLab. Can be `/api/v4` or `/api/v3`|
|`target_repo`|http://gogs.example.com|Gogs/Gitea URL|
|`gogs_token`|3240823dfsaefwio328923490832a|Gogs/Gitea accesstoken is used to access to target_repo. Note that the token must have the appropriate rights on Gogs\Gitea.|
|`private`|false|Repository will be private. Default is **false**|

# Next
Run script:
> `> migrate_from_gitlab_to_gogs.py config`

The script will automatically check the parameters from the config file. 

Displays the found projects and asks for confirmation of the migration.

# PS
### You can see that migration requires you to enter private data. By default, private data is transferred only between `source_repo` and `target_repo`. If you suspect that the script has been modified and your data may be compromised, please open `migrate_from_gitlab_to_gogs.py` in a text editor or IDE and make sure everything is correct.