
# encoding: utf-8
# author ka1ll

import argparse
import json
import os
import subprocess
import sys

import requests


def createParser ():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
    description=('''\
    CLI Script for automated migration form GitLab to Gogs with gogs migrate API
                    ------------------------------------
    Note:
    script_name.py [wizard] - run wizard

    script_name.py [config] - run script with params in config.json

    script_name.py [config] [-p] [specified_path/config.json] - run script with params from the specified folder
         '''))
    subparsers = parser.add_subparsers(dest='command')
 
    wizard_parser = subparsers.add_parser('wizard')
    # wizard_parser.add_argument ('--test', '-t', nargs='+', default=['test'])
 
    config_parser = subparsers.add_parser('config')
    config_parser.add_argument('-p', '--path', type=argparse.FileType(), default='config.json',
                                help="path with config.json. Default:[path_with_script/config.json]")
 
    return parser

def run_wizard(namespace):
    """
    Run wizard
    """
    print (f"Coming soon, {namespace}!")
 
 
 
def run_config(namespace):
    """
    Read and check params from config.json
    """
    config_file = json.loads(namespace.path.read())
    # check git_lab_api_version
    print('Check git_lab_api_version')
    if config_file['git_lab_api_version'] not in ['/api/v4', '/api/v4']:
        print("[git_lab_api_version must] be '/api/v4' or '/api/v3'")
        return False
    else:
        git_lab_api_version = config_file['git_lab_api_version']
        print("OK")
    # check source_type
    print('Check source_type')
    if config_file['source_type'] not in ['group', 'user']:
        print("[source_type] must be 'group' or 'user'")
        return False
    else:
        source_type = config_file['source_type']
        print("OK")
    # check source_type_name
    print('Check source_type_name')
    if not config_file['source_type_name']:
        print("Please past group name or user name")
        return False
    else:
        source_type_name = config_file['source_type_name']
        print("OK")
    # check source_repo
    try:
        print('Check source_repo')
        if requests.get(config_file['source_repo']).status_code != 200:
            print(f"{config_file['source_repo']} is not avalable now or wrong. [source_repo] must be as 'http[s]://gitlab.example.com'")
            return False
        else:
            source_repo = config_file['source_repo']
            gitlab_url = source_repo + git_lab_api_version
            print("OK")
    except Exception as e:
        print(e)
        return False
    # check gitlab_token
    print('Check gitlab_token')
    if not config_file['gitlab_token']:
        return "Paste your GitLab AccessToken. For example: 9koXpg98eAheJpvBs5tK"
    else:
        gitlab_token = config_file['gitlab_token']
        print("OK")
    # Check auth_username and auth_password
    print('Check auth_username and auth_password')
    if not config_file['auth_username'] or not config_file['auth_password']:
        print("Paste your GitLab user login and pass.\nThe user must have access rights to the source repository.")
        return False
    else:
        auth_username = config_file['auth_username']
        auth_password = config_file['auth_password']
        print("OK")
    # Check gitlab_token
    print('Check gogs_token')
    if not config_file['gogs_token']:
        print("Paste your Gogs AccessToken. For example: 3240823dfsaefwio328923490832a")
        return False
    else:
        gogs_token = config_file['gogs_token']
        print("OK")
    # Check target_repo
    try:
        print('Check target_repo')
        if requests.get(config_file['target_repo']).status_code != 200:
            print(f"{config_file['target_repo']} is not avalable now or wrong. [target_repo] must be as 'http[s]://gitlab.example.com'")
            return False
        else:
            target_repo = config_file['target_repo']
            print("OK")
    except Exception as e:
        print(e)
        return False
    # Check uid
    print('Check uid')
    try:
        uid_req = requests.get(target_repo + '/api/v1/users/' + source_type_name)
        if uid_req.status_code == 200:
            uid = json.loads(uid_req.text)['id']
            print('OK')
        else:
            print(f"User or Organization '{source_type_name}' not found. Please create it on Gogs")
            return False
    except Exception as e:
        print(e)
        return False

    # check private
    print('Check private')
    if config_file['private'] not in [True, False]:
        print("[private] must be 'true' or 'false'")
        return False
    else:
        private = config_file['private']
        print("OK")

    print('\nRun worker_for_gitlab\n')
    gitlab_params = dict(private_token = gitlab_token, simple = True, page = 1)
    prj_list = worker_for_gitlab(gitlab_url, gitlab_params, source_type_name, source_type)
   
    if prj_list:
        print('\nRun worker_for_gogs\n')
        gogs_params = dict(token=gogs_token, auth_username=auth_username, auth_password=auth_password, uid=uid, private=private)
        worker_for_gogs(target_repo, gogs_params, prj_list)
    else:
        print("Problem in worker_for_gitlab()")
    
def worker_for_gitlab(gitlab_url, gitlab_params, prj_namespace, source_type):
    s = requests.Session()
    finished = False
    project_list = list()
    while not finished:
        print(f"Getting page {gitlab_params['page']}")
        try:
            if source_type == 'group':
                res = s.get(gitlab_url + f"/groups/{prj_namespace}/projects", params=gitlab_params)
            elif source_type == 'user':
                res = s.get(gitlab_url + f"/users/{prj_namespace}/projects", params=gitlab_params)
            if res.status_code != 200:
                print(f"Error when retrieving the projects. The returned html is {res.text}")
                return False
            project_list.extend(json.loads(res.text))
            if len(json.loads(res.text)) < 1:
                finished = True
            else:
                gitlab_params['page'] += 1
        except Exception as e:
            print(e)
    print('\n\nFinished preparations. We are about to migrate the following projects:\n')
    [print(i + 1, p['path_with_namespace']) for i, p in enumerate(project_list)]
    return project_list

def worker_for_gogs(gogs_url, params, projects_list):
    gogs_url += '/api/v1/repos/migrate'
    s = requests.Session()
    print("What projects to migrate?\n\nExample:\nFor all projects input: all\nFor selected projects input your choice: 1 5 44 18\n")
    selected_projects = input().strip()
    if selected_projects.isalpha() or not selected_projects:
        if selected_projects != 'all':
            print("Wrong answer")
            return False
    else:
        selected_projects = list(map(int, selected_projects.split()))
        if max(selected_projects) > len(projects_list): 
            print("Wrong project numbers")
            return False
        projects_list = [prj for index, prj in enumerate(projects_list, start = 1) if index in selected_projects]

    for num in range(len(projects_list)):
        params['repo_name'] = projects_list[num]['path_with_namespace'].split('/')[1]
        params['clone_addr'] = projects_list[num]['http_url_to_repo']
        params['description'] = projects_list[num]['description']

        # Migrate repo with Gogs migrate API '/repos/migrate'
        print('\n\nMigrating project %s to project now.' % params['repo_name'])
        try:    
            create_repo = s.post(gogs_url, params)
        except Exception as e:
            print(e)
        if create_repo.status_code != 201:
            print('Status code', create_repo.status_code)
            print('Could not create repo %s because of %s' % (params['repo_name'], create_repo.text))
            if input('Do you want to skip this repo and continue with the next? (please answer yes[y] for continue or other to cancel)') not in ['yes','y']:
                print('\nYou decided to cancel...')
                exit(1)
            continue
        print('OK')
    print('Done!')

if __name__ == '__main__':
        parser = createParser()
        namespace = parser.parse_args(sys.argv[1:])
        if namespace.command == "config":
            run_config(namespace)
        elif namespace.command == "wizard":
            run_wizard(namespace)
        else:
            parser.print_help()
